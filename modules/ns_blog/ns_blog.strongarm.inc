<?php

/**
 * Implementation of hook_strongarm().
 */
function ns_blog_strongarm() {
  $export = array();
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_ns_blog';
  $strongarm->value = array(
    'title' => '-5',
    'revision_information' => '4',
    'author' => '5',
    'options' => '0',
    'menu' => '2',
    'path' => '3',
    'scheduler_settings' => '1',
  );

  $export['content_extra_weights_ns_blog'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_ns_post';
  $strongarm->value = array(
    'title' => '-5',
    'revision_information' => '4',
    'author' => '5',
    'options' => '0',
    'menu' => '2',
    'path' => '3',
    'scheduler_settings' => '1',
  );

  $export['content_extra_weights_ns_post'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_ns_blog';
  $strongarm->value = array(
    0 => 'status',
  );

  $export['node_options_ns_blog'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_ns_post';
  $strongarm->value = array(
    0 => 'status',
  );

  $export['node_options_ns_post'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_ns_blog';
  $strongarm->value = 1;

  $export['scheduler_ns_blog'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_ns_post';
  $strongarm->value = 1;

  $export['scheduler_ns_post'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_touch_ns_blog';
  $strongarm->value = 1;

  $export['scheduler_touch_ns_blog'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_touch_ns_post';
  $strongarm->value = 1;

  $export['scheduler_touch_ns_post'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_diff_inline_ns_blog';
  $strongarm->value = 0;

  $export['show_diff_inline_ns_blog'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_diff_inline_ns_post';
  $strongarm->value = 0;

  $export['show_diff_inline_ns_post'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_preview_changes_ns_blog';
  $strongarm->value = 1;

  $export['show_preview_changes_ns_blog'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_preview_changes_ns_post';
  $strongarm->value = 1;

  $export['show_preview_changes_ns_post'] = $strongarm;
  return $export;
}
