<?php

/**
 * Implementation of hook_input_formats().
 */
function ns_input_formats_input_formats() {
  $input_formats = array();

  $input_format = new stdClass;
  $input_format->api_version = 1;
  $input_format->name = 'Text editor';
  $input_format->roles = array(
    3 => 'administrator',
    4 => 'editor',
    5 => 'writer',
  );
  $input_format->cache = TRUE;
  $input_format->settings = array(
    // Settings for: filter module
    'filter' => array(
      'filter_html' => '1',
      'allowed_html' => '<a> <em> <strong> <cite> <code> <ul> <ol> <li> <p> <h2> <h3> <h4> <h5> <h6>',
      'filter_html_help' => 1,
      'filter_html_nofollow' => 0,
    ),
  );
  $input_format->filters = array(
    'filter' => array(
      '0' => '10',
      '3' => '10',
    ),
  );
  $input_format->machine = 'text_editor';
  $input_formats['text_editor'] = $input_format;

  return $input_formats;

}
