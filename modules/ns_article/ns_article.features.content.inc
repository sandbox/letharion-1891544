<?php

/**
 * Implementation of hook_content_default_fields().
 */
function ns_article_content_default_fields() {
  $fields = array();

  // Exported field: field_ns_article_attachment
  $fields['ns_article-field_ns_article_attachment'] = array(
    'field_name' => 'field_ns_article_attachment',
    'type_name' => 'ns_article',
    'display_settings' => array(
      'weight' => 0,
      'parent' => '',
      'label' => array(
        'format' => 'hidden',
      ),
      'teaser' => array(
        'format' => 'hidden',
        'exclude' => 1,
      ),
      'full' => array(
        'format' => 'hidden',
        'exclude' => 1,
      ),
      '5' => array(
        'format' => 'hidden',
        'exclude' => 1,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '0',
    'multiple' => '1',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'ns_fact' => 'ns_fact',
      'ns_form' => 'ns_form',
      'ns_location' => 'ns_location',
      'poll' => 'poll',
      'ns_video' => 'ns_video',
      'ns_article' => 0,
      'ns_blurb' => 0,
      'ns_contributor' => 0,
      'ns_image' => 0,
      'ns_page' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'widget' => array(
      'autocomplete_match' => 'contains',
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'nid' => NULL,
          '_error_element' => 'default_value_widget][field_ns_article_attachment][0][nid][nid',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Attachments',
      'weight' => 0,
      'description' => '',
      'type' => 'nodereference_autocomplete',
      'module' => 'nodereference',
    ),
  );

  // Exported field: field_ns_article_body
  $fields['ns_article-field_ns_article_body'] = array(
    'field_name' => 'field_ns_article_body',
    'type_name' => 'ns_article',
    'display_settings' => array(
      'weight' => '-2',
      'parent' => '',
      'label' => array(
        'format' => 'hidden',
      ),
      'teaser' => array(
        'format' => 'hidden',
        'exclude' => 1,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'aside' => array(
        'format' => 'hidden',
        'exclude' => 1,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '1',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => '10',
      'size' => 60,
      'default_value' => array(
        '0' => array(
          'value' => '',
          'format' => '3',
          '_error_element' => 'default_value_widget][field_ns_article_body][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Body',
      'weight' => '-2',
      'description' => '',
      'type' => 'text_textarea',
      'module' => 'text',
    ),
  );

  // Exported field: field_ns_article_byline
  $fields['ns_article-field_ns_article_byline'] = array(
    'field_name' => 'field_ns_article_byline',
    'type_name' => 'ns_article',
    'display_settings' => array(
      'weight' => '1',
      'parent' => '',
      'label' => array(
        'format' => 'hidden',
      ),
      'teaser' => array(
        'format' => 'hidden',
        'exclude' => 1,
      ),
      'full' => array(
        'format' => 'hidden',
        'exclude' => 1,
      ),
      '5' => array(
        'format' => 'plain',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'aside' => array(
        'format' => 'hidden',
        'exclude' => 1,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '0',
    'multiple' => '1',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'ns_contributor' => 'ns_contributor',
      'ns_article' => 0,
      'ns_blurb' => 0,
      'ns_fact' => 0,
      'ns_form' => 0,
      'ns_image' => 0,
      'ns_location' => 0,
      'ns_page' => 0,
      'ns_video' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'widget' => array(
      'autocomplete_match' => 'contains',
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'nid' => NULL,
          '_error_element' => 'default_value_widget][field_ns_article_byline][0][nid][nid',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Byline',
      'weight' => '1',
      'description' => '',
      'type' => 'nodereference_autocomplete',
      'module' => 'nodereference',
    ),
  );

  // Exported field: field_ns_article_image
  $fields['ns_article-field_ns_article_image'] = array(
    'field_name' => 'field_ns_article_image',
    'type_name' => 'ns_article',
    'display_settings' => array(
      'weight' => '-1',
      'parent' => '',
      'label' => array(
        'format' => 'hidden',
      ),
      'teaser' => array(
        'format' => 'hidden',
        'exclude' => 1,
      ),
      'full' => array(
        'format' => 'hidden',
        'exclude' => 1,
      ),
      '5' => array(
        'format' => 'hidden',
        'exclude' => 1,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'aside' => array(
        'format' => 'hidden',
        'exclude' => 1,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '0',
    'multiple' => '1',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'ns_image' => 'ns_image',
      'ns_article' => 0,
      'ns_audio' => 0,
      'book' => 0,
      'ns_external' => 0,
      'ns_fact' => 0,
      'ns_form' => 0,
      'ns_manual' => 0,
      'ns_page' => 0,
      'ns_location' => 0,
      'ns_local_banner' => 0,
      'ns_link' => 0,
      'ns_contributor' => 0,
      'ns_poll' => 0,
      'ns_blurb' => 0,
      'ns_correction' => 0,
      'ns_table' => 0,
      'ns_video' => 0,
      'ns_waby' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'widget' => array(
      'autocomplete_match' => 'contains',
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'nid' => NULL,
          '_error_element' => 'default_value_widget][field_ns_article_image][0][nid][nid',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Images',
      'weight' => '-1',
      'description' => '',
      'type' => 'nodereference_autocomplete',
      'module' => 'nodereference',
    ),
  );

  // Exported field: field_ns_article_kicker
  $fields['ns_article-field_ns_article_kicker'] = array(
    'field_name' => 'field_ns_article_kicker',
    'type_name' => 'ns_article',
    'display_settings' => array(
      'weight' => '-4',
      'parent' => '',
      'label' => array(
        'format' => 'hidden',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_ns_article_kicker][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Kicker',
      'weight' => '-4',
      'description' => '',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_ns_article_lead
  $fields['ns_article-field_ns_article_lead'] = array(
    'field_name' => 'field_ns_article_lead',
    'type_name' => 'ns_article',
    'display_settings' => array(
      'weight' => '-3',
      'parent' => '',
      'label' => array(
        'format' => 'hidden',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'aside' => array(
        'format' => 'trimmed',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '1',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => '4',
      'size' => 60,
      'default_value' => array(
        '0' => array(
          'value' => '',
          'format' => '3',
          '_error_element' => 'default_value_widget][field_ns_article_lead][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Lead',
      'weight' => '-3',
      'description' => '',
      'type' => 'text_textarea',
      'module' => 'text',
    ),
  );

  // Exported field: field_ns_promo_article
  $fields['ns_promo-field_ns_promo_article'] = array(
    'field_name' => 'field_ns_promo_article',
    'type_name' => 'ns_promo',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '1',
    'multiple' => '0',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'ns_article' => 'ns_article',
      'ad' => 0,
      'ns_blog' => 0,
      'ns_blurb' => 0,
      'ns_contributor' => 0,
      'ns_fact' => 0,
      'ns_form' => 0,
      'ns_image' => 0,
      'ns_page' => 0,
      'poll' => 0,
      'ns_post' => 0,
      'ns_promo' => 0,
      'ns_video' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'widget' => array(
      'autocomplete_match' => 'contains',
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'nid' => NULL,
          '_error_element' => 'default_value_widget][field_ns_promo_article][0][nid][nid',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Article',
      'weight' => '4',
      'description' => '',
      'type' => 'nodereference_autocomplete',
      'module' => 'nodereference',
    ),
  );

  // Exported field: field_ns_promo_body
  $fields['ns_promo-field_ns_promo_body'] = array(
    'field_name' => 'field_ns_promo_body',
    'type_name' => 'ns_promo',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '1',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => '4',
      'size' => 60,
      'default_value' => array(
        '0' => array(
          'value' => '',
          'format' => '3',
          '_error_element' => 'default_value_widget][field_ns_promo_body][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Body',
      'weight' => '7',
      'description' => '',
      'type' => 'text_textarea',
      'module' => 'text',
    ),
  );

  // Exported field: field_ns_promo_image
  $fields['ns_promo-field_ns_promo_image'] = array(
    'field_name' => 'field_ns_promo_image',
    'type_name' => 'ns_promo',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '0',
    'multiple' => '0',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'ns_image' => 'ns_image',
      'ad' => 0,
      'ns_article' => 0,
      'ns_blog' => 0,
      'ns_blurb' => 0,
      'ns_contributor' => 0,
      'ns_fact' => 0,
      'ns_form' => 0,
      'ns_page' => 0,
      'poll' => 0,
      'ns_post' => 0,
      'ns_promo' => 0,
      'ns_video' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'widget' => array(
      'autocomplete_match' => 'contains',
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'nid' => NULL,
          '_error_element' => 'default_value_widget][field_ns_promo_image][0][nid][nid',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Image',
      'weight' => '8',
      'description' => '',
      'type' => 'nodereference_autocomplete',
      'module' => 'nodereference',
    ),
  );

  // Exported field: field_ns_promo_kicker
  $fields['ns_promo-field_ns_promo_kicker'] = array(
    'field_name' => 'field_ns_promo_kicker',
    'type_name' => 'ns_promo',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_ns_promo_kicker][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Kicker',
      'weight' => '6',
      'description' => '',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_ns_promo_parent
  $fields['ns_promo-field_ns_promo_parent'] = array(
    'field_name' => 'field_ns_promo_parent',
    'type_name' => 'ns_promo',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '0',
    'multiple' => '0',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'ns_promo' => 'ns_promo',
      'ad' => 0,
      'ns_article' => 0,
      'ns_blog' => 0,
      'ns_blurb' => 0,
      'ns_contributor' => 0,
      'ns_fact' => 0,
      'ns_form' => 0,
      'ns_image' => 0,
      'ns_page' => 0,
      'poll' => 0,
      'ns_post' => 0,
      'ns_video' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'widget' => array(
      'autocomplete_match' => 'contains',
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'nid' => NULL,
          '_error_element' => 'default_value_widget][field_ns_promo_parent][0][nid][nid',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Parent promo',
      'weight' => '9',
      'description' => '',
      'type' => 'nodereference_autocomplete',
      'module' => 'nodereference',
    ),
  );

  // Exported field: field_ns_promo_style
  $fields['ns_promo-field_ns_promo_style'] = array(
    'field_name' => 'field_ns_promo_style',
    'type_name' => 'ns_promo',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'dynamic_formatters_reference',
    'required' => '0',
    'multiple' => '0',
    'module' => 'dynamic_formatters',
    'active' => '1',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'style' => 'ns_main_full',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Style',
      'weight' => '10',
      'description' => '',
      'type' => 'dynamic_formatters_select',
      'module' => 'dynamic_formatters',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Article');
  t('Attachments');
  t('Body');
  t('Byline');
  t('Image');
  t('Images');
  t('Kicker');
  t('Lead');
  t('Parent promo');
  t('Style');

  return $fields;
}
