<?php

/**
 * Implementation of hook_user_default_roles().
 */
function ns_article_user_default_roles() {
  $roles = array();

  // Exported role: administrator
  $roles['administrator'] = array(
    'name' => 'administrator',
  );

  // Exported role: editor
  $roles['editor'] = array(
    'name' => 'editor',
  );

  // Exported role: writer
  $roles['writer'] = array(
    'name' => 'writer',
  );

  return $roles;
}
