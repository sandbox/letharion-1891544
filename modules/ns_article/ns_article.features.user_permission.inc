<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function ns_article_user_default_permissions() {
  $permissions = array();

  // Exported permission: administer topics
  $permissions['administer topics'] = array(
    'name' => 'administer topics',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'editor',
    ),
  );

  // Exported permission: create ns_article content
  $permissions['create ns_article content'] = array(
    'name' => 'create ns_article content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'writer',
    ),
  );

  // Exported permission: create ns_promo content
  $permissions['create ns_promo content'] = array(
    'name' => 'create ns_promo content',
    'roles' => array(),
  );

  // Exported permission: delete any ns_article content
  $permissions['delete any ns_article content'] = array(
    'name' => 'delete any ns_article content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: delete any ns_promo content
  $permissions['delete any ns_promo content'] = array(
    'name' => 'delete any ns_promo content',
    'roles' => array(),
  );

  // Exported permission: delete own ns_article content
  $permissions['delete own ns_article content'] = array(
    'name' => 'delete own ns_article content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: delete own ns_promo content
  $permissions['delete own ns_promo content'] = array(
    'name' => 'delete own ns_promo content',
    'roles' => array(),
  );

  // Exported permission: edit any ns_article content
  $permissions['edit any ns_article content'] = array(
    'name' => 'edit any ns_article content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: edit any ns_promo content
  $permissions['edit any ns_promo content'] = array(
    'name' => 'edit any ns_promo content',
    'roles' => array(),
  );

  // Exported permission: edit own ns_article content
  $permissions['edit own ns_article content'] = array(
    'name' => 'edit own ns_article content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'writer',
    ),
  );

  // Exported permission: edit own ns_promo content
  $permissions['edit own ns_promo content'] = array(
    'name' => 'edit own ns_promo content',
    'roles' => array(),
  );

  return $permissions;
}
