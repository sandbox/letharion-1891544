<?php

/**
 * Implementation of hook_openlayers_presets().
 */
function ns_location_openlayers_presets() {
  $export = array();
  $openlayers_presets = new stdClass;
  $openlayers_presets->disabled = FALSE; /* Edit this to true to make a default openlayers_presets disabled initially */
  $openlayers_presets->api_version = 1;
  $openlayers_presets->name = 'ns_article_attachment';
  $openlayers_presets->title = 'NS article attachment';
  $openlayers_presets->description = 'This is the preset used as attachment to articles.';
  $openlayers_presets->data = array(
    'width' => '170px',
    'height' => '170px',
    'image_path' => '',
    'css_path' => '',
    'proxy_host' => '',
    'center' => array(
      'initial' => array(
        'centerpoint' => '18.017578124281, 54.367758522556',
        'zoom' => '3',
      ),
      'restrict' => array(
        'restrictextent' => 0,
        'restrictedExtent' => '',
      ),
    ),
    'behaviors' => array(
      'openlayers_behavior_argparser' => array(),
      'openlayers_behavior_popup' => array(),
      'openlayers_behavior_tooltip' => array(),
      'openlayers_behavior_navigation' => array(
        'zoomWheelEnabled' => 0,
      ),
      'openlayers_behavior_dragpan' => array(),
      'openlayers_behavior_panzoom' => array(),
      'openlayers_behavior_permalink' => array(),
      'openlayers_behavior_zoomtolayer' => array(
        'zoomtolayer' => 'ns_article_location_openlayers_1',
        'point_zoom_level' => '10',
      ),
    ),
    'default_layer' => 'osm_mapnik',
    'layers' => array(
      'osm_mapnik' => 'osm_mapnik',
      'ns_article_location_openlayers_1' => 'ns_article_location_openlayers_1',
    ),
    'layer_styles' => array(),
    'layer_activated' => array(
      'ns_article_location_openlayers_1' => 'ns_article_location_openlayers_1',
    ),
    'layer_switcher' => array(
      'ns_article_location_openlayers_1' => 'ns_article_location_openlayers_1',
    ),
    'projection' => '900913',
    'displayProjection' => '4326',
    'styles' => array(
      'default' => 'default',
      'select' => 'default',
      'temporary' => 'default',
    ),
    'options' => NULL,
  );

  $export['ns_article_attachment'] = $openlayers_presets;
  $openlayers_presets = new stdClass;
  $openlayers_presets->disabled = FALSE; /* Edit this to true to make a default openlayers_presets disabled initially */
  $openlayers_presets->api_version = 1;
  $openlayers_presets->name = 'ns_node_form';
  $openlayers_presets->title = 'NS node form';
  $openlayers_presets->description = 'This is the preset used in node forms.';
  $openlayers_presets->data = array(
    'width' => 'auto',
    'height' => '400px',
    'image_path' => '',
    'css_path' => '',
    'proxy_host' => '',
    'center' => array(
      'initial' => array(
        'centerpoint' => '23.378906249065, 56.992882803132',
        'zoom' => '3',
      ),
      'restrict' => array(
        'restrictextent' => 0,
        'restrictedExtent' => '',
      ),
    ),
    'behaviors' => array(
      'openlayers_behavior_navigation' => array(
        'zoomWheelEnabled' => 0,
      ),
      'openlayers_behavior_dragpan' => array(),
      'openlayers_behavior_panzoombar' => array(),
    ),
    'default_layer' => 'osm_mapnik',
    'layers' => array(
      'osm_mapnik' => 'osm_mapnik',
    ),
    'layer_styles' => array(
      'ns_article_attachment_openlayers_1' => 'default',
    ),
    'layer_activated' => array(),
    'layer_switcher' => array(),
    'projection' => '900913',
    'displayProjection' => '4326',
    'styles' => array(
      'default' => 'default',
      'select' => 'default',
      'temporary' => 'default',
    ),
    'options' => NULL,
  );

  $export['ns_node_form'] = $openlayers_presets;
  return $export;
}
