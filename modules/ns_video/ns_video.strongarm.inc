<?php

/**
 * Implementation of hook_strongarm().
 */
function ns_video_strongarm() {
  $export = array();
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_ns_video';
  $strongarm->value = array(
    'title' => '-5',
    'revision_information' => '3',
    'author' => '4',
    'options' => '-1',
    'menu' => '1',
    'path' => '2',
    'scheduler_settings' => '0',
  );

  $export['content_extra_weights_ns_video'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'enable_revisions_page_ns_video';
  $strongarm->value = 1;

  $export['enable_revisions_page_ns_video'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_ns_video';
  $strongarm->value = array(
    0 => 'status',
  );

  $export['node_options_ns_video'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_ns_video';
  $strongarm->value = 1;

  $export['scheduler_ns_video'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_touch_ns_video';
  $strongarm->value = 1;

  $export['scheduler_touch_ns_video'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_diff_inline_ns_video';
  $strongarm->value = 0;

  $export['show_diff_inline_ns_video'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_preview_changes_ns_video';
  $strongarm->value = 1;

  $export['show_preview_changes_ns_video'] = $strongarm;
  return $export;
}
