<?php

$plugin = array(
  'title' => t('Primary links only'),
  'single' => TRUE,
  'icon' => 'icon_page.png',
  'description' => t('Outputs primary links without secondary'),
  'category' => t('Page elements'),
);

function ns_core_primary_links_only_content_type_render($subtype, $conf, $panel_args) {
  $primary_links = menu_primary_links();
	$block = new stdClass();
  $block->content = theme('links', $primary_links);
  return $block;
}

function ns_core_primary_links_only_content_type_admin_info($subtype, $conf) {
  $block->title = t('Primary links only');
  $block->content = t('Outputs primary links without secondary.');
  return $block;
}
