<?php

/**
 * Implementation of hook_strongarm().
 */
function ns_contributor_strongarm() {
  $export = array();
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_ns_contributor';
  $strongarm->value = array(
    'title' => '-5',
    'revision_information' => '2',
    'author' => '3',
    'options' => '-2',
    'menu' => '0',
    'path' => '1',
    'scheduler_settings' => '-1',
  );

  $export['content_extra_weights_ns_contributor'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'enable_revisions_page_ns_contributor';
  $strongarm->value = 1;

  $export['enable_revisions_page_ns_contributor'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_ns_contributor';
  $strongarm->value = array(
    0 => 'status',
  );

  $export['node_options_ns_contributor'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rdf_schema_class_ns_contributor';
  $strongarm->value = '';

  $export['rdf_schema_class_ns_contributor'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_ns_contributor';
  $strongarm->value = 1;

  $export['scheduler_ns_contributor'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_touch_ns_contributor';
  $strongarm->value = 1;

  $export['scheduler_touch_ns_contributor'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_diff_inline_ns_contributor';
  $strongarm->value = 0;

  $export['show_diff_inline_ns_contributor'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_preview_changes_ns_contributor';
  $strongarm->value = 1;

  $export['show_preview_changes_ns_contributor'] = $strongarm;
  return $export;
}
