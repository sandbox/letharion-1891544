<?php

/**
 * Implementation of hook_strongarm().
 */
function ns_image_strongarm() {
  $export = array();
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_ns_image';
  $strongarm->value = array(
    'title' => '-5',
    'revision_information' => '4',
    'author' => '5',
    'options' => '1',
    'menu' => '2',
    'path' => '3',
  );

  $export['content_extra_weights_ns_image'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'enable_revisions_page_ns_image';
  $strongarm->value = 1;

  $export['enable_revisions_page_ns_image'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_ns_image';
  $strongarm->value = array(
    0 => 'status',
  );

  $export['node_options_ns_image'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rdf_schema_class_ns_image';
  $strongarm->value = '';

  $export['rdf_schema_class_ns_image'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_ns_image';
  $strongarm->value = 0;

  $export['scheduler_ns_image'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_touch_ns_image';
  $strongarm->value = 0;

  $export['scheduler_touch_ns_image'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_diff_inline_ns_image';
  $strongarm->value = 0;

  $export['show_diff_inline_ns_image'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_preview_changes_ns_image';
  $strongarm->value = 1;

  $export['show_preview_changes_ns_image'] = $strongarm;
  return $export;
}
