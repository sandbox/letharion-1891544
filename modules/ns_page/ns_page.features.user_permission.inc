<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function ns_page_user_default_permissions() {
  $permissions = array();

  // Exported permission: create ns_page content
  $permissions['create ns_page content'] = array(
    'name' => 'create ns_page content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: delete any ns_page content
  $permissions['delete any ns_page content'] = array(
    'name' => 'delete any ns_page content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: delete own ns_page content
  $permissions['delete own ns_page content'] = array(
    'name' => 'delete own ns_page content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: edit any ns_page content
  $permissions['edit any ns_page content'] = array(
    'name' => 'edit any ns_page content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: edit own ns_page content
  $permissions['edit own ns_page content'] = array(
    'name' => 'edit own ns_page content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  return $permissions;
}
