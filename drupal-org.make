; $Id$

; Core

core = "6.19"

; Modules

projects[ad][subdir] = "contrib"
projects[ad][version] = "2.2"

projects[admin][subdir] = "contrib"
projects[admin][version] = "2.0-rc1"

projects[cck][subdir] = "contrib"
projects[cck][version] = "2.8"

projects[chart][subdir] = "contrib"
projects[chart][version] = "1.2"

projects[conditional_fields] = "contrib"
projects[conditional_fields] = "2.0-beta1"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.7"

projects[diff][subdir] = "contrib"
projects[diff][version] = "2.1"

projects[disqus][subdir] = "contrib"
projects[disqus][version] = "1.6"

projects[draggableviews][subdir] = "contrib"
projects[draggableviews][version] = "3.4"

projects[dynamic_formatters][subdir] = "contrib"
projects[dynamic_formatters][version] = "1.0-alpha1"

projects[email][subdir] = "contrib"
projects[email][version] = "1.2"

projects[emfield][subdir] = "contrib"
projects[emfield][version] = "1.24"

projects[exportables][subdir] = "contrib"
projects[exportables][version] = "2.0-beta1"

projects[features][subdir] = "contrib"
projects[features][version] = "1.0-rc1"

projects[filefield][subdir] = "contrib"
projects[filefield][version] = "3.7"

projects[geotaxonomy][subdir] = "contrib"
projects[geotaxonomy][version] = "2.0-beta2"

projects[input_formats][subdir] = "contrib"
projects[input_formats][version] = "1.0-beta3"

projects[imageapi][subdir] = "contrib"
projects[imageapi][version] = "1.8"

projects[imagecache][subdir] = "contrib"
projects[imagecache][version] = "2.0-beta10"

projects[imagefield][subdir] = "contrib"
projects[imagefield][version] = "3.7"

projects[jquery_ui][subdir] = "contrib"
projects[jquery_ui][version] = "1.3"

projects[jquery_update][subdir] = "contrib"
projects[jquery_update][version] = "2.0-alpha1"

projects[modalframe][subdir] = "contrib"
projects[modalframe][version] = "1.7"

projects[noderelationships][subdir] = "contrib"
projects[noderelationships][version] = "1.6"

projects[openlayers][subdir] = "contrib"
projects[openlayers][version] = "2.0-alpha8"

projects[panels][subdir] = "contrib"
projects[panels][version] = "3.7"

projects[panels_everywhere][subdir] = "contrib"
projects[panels_everywhere][version] = "1.1"

projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = "1.4"

projects[scheduler][subdir] = "contrib"
projects[scheduler][version] = "1.7"

projects[semanticviews][subdir] = "contrib"
projects[semanticviews][version] = "1.1"

projects[strongarm][subdir] = "contrib"
projects[strongarm][version] = "2.0"

projects[token][subdir] = "contrib"
projects[token][version] = "1.14"

projects[twitter][subdir] = "contrib"
projects[twitter][version] = "2.6"

projects[vertical_tabs][subdir] = "contrib"
projects[vertical_tabs][version] = "1.0-rc1"

projects[views][subdir] = "contrib"
projects[views][version] = "2.11"

projects[views_slideshow][subdir] = "contrib"
projects[views_slideshow][version] = "2.3"

projects[webform][subdir] = "contrib"
projects[webform][version] = "3.1"

projects[wysiwyg][subdir] = "contrib"
projects[wysiwyg][version] = "2.1"

; Development

projects[devel][subdir] = "developer"
projects[devel][version] = "1.22"

; Themes

projects[precision][version] = "1.0-alpha1"

projects[seven][version] = "1.0-beta10"
