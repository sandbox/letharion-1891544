<?php
?>
<div class="<?php print $classes; ?> clear-block">
  <?php if (!$page && !empty($title)): ?>
    <h2 class="node-title"><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>

  <?php print $content; ?>

  <?php if (!empty($links)): ?>
    <div class="node-links">
      <?php print $links; ?>
    </div>
  <?php endif; ?>
</div>
