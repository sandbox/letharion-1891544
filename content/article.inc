<?php

$base = array(
  'type' => 'ns_article',
  'language' => '',
  'uid' => $user->uid,
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'moderate' => 0,
  'sticky' => 0,
  'tnid' => 0,
  'translate' => 0,
  'revision_uid' => $user->uid,
  'format' => 0,
  'name' => $user->name,
);

$nodes = array(
  $base + array(
    'title' => 'Placeholder',
    'field_ns_article_body' => array(
      0 => array(
        'value' => '<p>This is placeholder content.</p>',
        'format' => 3,
      ),
    ),
    'field_ns_article_lead' => array(
      0 => array(
        'value' => NULL,
        'format' => 3,
      ),
    ),
    'field_ns_article_image' => array(
      0 => array(
        'nid' => NULL,
      ),
    ),
    'field_ns_article_attachment' => array(
      0 => array(
        'nid' => NULL,
      ),
    ),
    'field_ns_article_byline' => array(
      0 => array(
        'nid' => 1,
      ),
    ),
    'taxonomy' => array(
      1 => array(
        2 => 2,
      ),
    ),
  )
);
