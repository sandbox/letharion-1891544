<?php

$base = array(
  'type' => 'ns_promo',
  'language' => '',
  'uid' => $user->uid,
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'moderate' => 0,
  'sticky' => 0,
  'tnid' => 0,
  'translate' => 0,
  'revision_uid' => $user->uid,
  'format' => 0,
  'name' => $user->name,
);

$nodes = array(
  $base + array(
    'title' => 'Placeholder',
    'field_ns_promo_article' => array(
      0 => array(
        'nid' => 2,
      ),
    ),
    'field_ns_promo_body' => array(
      0 => array(
        'value' => '<p>This is placeholder content.</p>',
        'format' => 3,
      ),
    ),
    'field_ns_promo_image' => array(
      0 => array(
        'nid' => NULL,
      ),
    ),
    'field_ns_promo_parent' => array(
      0 => array(
        'nid' => NULL,
      ),
    ),
    'field_ns_promo_style' => array(
      0 => array(
        'style' => 'ns_main_full',
      ),
    ),
    'taxonomy' => array(
      1 => array(
        1 => 1,
        2 => 2,
      ),
      2 => array(
        5 => 5,
      ),
    ),
  ),
);
