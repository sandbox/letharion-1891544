<?php

$base = array(
  'type' => 'ns_contributor',
  'language' => '',
  'uid' => $user->uid,
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'moderate' => 0,
  'sticky' => 0,
  'tnid' => 0,
  'translate' => 0,
  'revision_uid' => $user->uid,
  'format' => 0,
  'name' => $user->name,
);

$nodes = array(
  $base + array(
    'title' => 'NodeOne',
    'field_ns_contributor_email' => array(
      0 => array(
        'email' => 'info@nodeone.se',
      ),
    ),
    'field_ns_contributor_photo' => array(
      0 => NULL,
    ),
    'taxonomy' => array(),
  )
);
