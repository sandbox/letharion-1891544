<?php

$vocabularies = array(
  array(
    'name' => 'Topic',
    'relations' => TRUE,
    'hierarchy' => FALSE,
    'multiple' => TRUE,
    'required' => TRUE,
    'tags' => FALSE,
    'module' => 'taxonomy',
    'weight' => -10,
    'nodes' => array(
      'ns_article' => TRUE,
      'ns_promo' => TRUE,
    ),
  ),
  array(
    'name' => 'Topic region',
    'relations' => TRUE,
    'hierarchy' => TRUE,
    'multiple' => FALSE,
    'required' => TRUE,
    'tags' => FALSE,
    'module' => 'taxonomy',
    'weight' => -9,
    'nodes' => array(
      'ns_promo' => TRUE,
    ),
  ),
  array(
    'name' => 'Geographic location',
    'relations' => TRUE,
    'hierarchy' => TRUE,
    'multiple' => TRUE,
    'required' => FALSE,
    'tags' => FALSE,
    'module' => 'taxonomy',
    'weight' => -8,
    'nodes' => array(
      'ns_article' => TRUE,
    ),
  ),
  array(
    'name' => 'Ad groups',
    'multiple' => TRUE,
    'required' => FALSE,
    'hierarchy' => FALSE,
    'relations' => FALSE,
    'module' => 'ad',
    'weight' => -7,
    'nodes' => array(
      'ad' => TRUE,
    ),
  ),
);
