<?php

$terms = array(
  array(
    'name' => 'Front page',
    'vid' => 1,
    'weight' => -10,
  ),
  array(
    'name' => 'Business',
    'vid' => 1,
    'weight' => -9,
  ),
  array(
    'name' => 'Politics',
    'vid' => 1,
    'weight' => -8,
  ),
  array(
    'name' => 'Technology',
    'vid' => 1,
    'weight' => -10,
  ),
  array(
    'name' => 'Main',
    'vid' => 2,
    'weight' => -10,
  ),
  array(
    'name' => 'Header full',
    'vid' => 2,
    'weight' => -9,
    'parent' => array(5),
  ),
  array(
    'name' => 'Header half',
    'vid' => 2,
    'weight' => -8,
    'parent' => array(5),
  ),
  array(
    'name' => 'Aside',
    'vid' => 2,
    'weight' => -7,
  ),
  array(
    'name' => 'Slideshow',
    'vid' => 2,
    'weight' => -6,
    'parent' => array(8),
  ),
  array(
    'name' => 'Header (980 px)',
    'vid' => 4,
    'weight' => -10,
  ),
  array(
    'name' => 'Column (250 px)',
    'vid' => 4,
    'weight' => -9,
  ),
);
